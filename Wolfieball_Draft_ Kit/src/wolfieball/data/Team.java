/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wolfieball.data;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author shilinlu
 */
public class Team {
    private String name;
    ObservableList<Hitters> hitters;
    ObservableList<Pitchers> pitchers;
    private String owner;
public Team(){
    
    hitters= FXCollections.observableArrayList();
    pitchers= FXCollections.observableArrayList();
}

public String getOwner(){
    return owner;
}
public void setOwner(String owner){
    this.owner=owner;
}

public void setName(String name){
    this.name=name;
}

public String getName(){
    return name;
}

public ObservableList<Hitters> getHitters() {
        return hitters;
    }

public ObservableList<Pitchers> getPitchers() {
        return pitchers;
    }
public void setHitters(ObservableList<Hitters> hitters){
    this.hitters=hitters;
    }
public void setPitchers(ObservableList<Pitchers> pitchers){
    this.pitchers=pitchers;
    }

public void addHitters(Hitters l) {
        hitters.add(l);
    }
public void addPitchers(Pitchers l) {
        pitchers.add(l);
    }
public void addPlayer(Object player){
    if(player instanceof Hitters){
        hitters.add((Hitters)player);
    }
    if(player instanceof Pitchers){
        pitchers.add((Pitchers)player);
    }

}

public void removeHitter(Hitters itemToRemove) {
        hitters.remove(itemToRemove);
    }
public void removePlayer(Object player){
    if(player instanceof Hitters){
        hitters.remove(player);
    }
    if(player instanceof Pitchers){
        pitchers.remove(player);
    }

}

}
