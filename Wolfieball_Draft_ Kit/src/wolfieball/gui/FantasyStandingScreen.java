/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wolfieball.gui;

import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;

import wolfieball_draft_kit.PropertyType;

/**
 *
 * @author shilinlu
 */
public class FantasyStandingScreen {
    BorderPane workSpacePane;
    VBox topWorkSpacePane;
    VBox fantasyStandingsPane;
    VBox fantasyStandingsItemsBox;
    ScrollPane workSpaceScrollPane;
    SplitPane topWorkSpaceSplitPane;
    
    Label fantasyStandingLabel;
    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_HEADING_LABEL = "heading_label";
    
    
    FantasyStandingScreen(){
        
    
    }
    public void initEverything(){
        workSpacePane= new BorderPane();
        topWorkSpacePane = new VBox();
        fantasyStandingsPane = new VBox();
        workSpaceScrollPane= new ScrollPane();
        topWorkSpaceSplitPane= new SplitPane();
        workSpacePane.setTop(topWorkSpacePane);
        workSpacePane.setCenter(fantasyStandingsPane);
        workSpacePane.getStyleClass().add(CLASS_BORDERED_PANE);
        
        // AND NOW PUT IT IN THE WORKSPACE
        workSpaceScrollPane = new ScrollPane();
        workSpaceScrollPane.setContent(workSpacePane);
        workSpaceScrollPane.setFitToWidth(true);
        fantasyStandingLabel= initChildLabel(topWorkSpacePane, PropertyType.FANTASY_STANDINGS_LABEL, CLASS_HEADING_LABEL);
        // AND NOW ADD THE SPLIT PANE
        topWorkSpacePane.getChildren().add(topWorkSpaceSplitPane);
        fantasyStandingsItemsBox = new VBox();
        
        fantasyStandingsPane.setMinSize(400, 600);
        fantasyStandingsPane.getChildren().add(fantasyStandingsItemsBox);
        TableView standingsTable=new TableView();
        TableColumn teamName= new TableColumn("Team Name");TableColumn playersNeeded= new TableColumn("Players Needed"); 
        TableColumn moneyLeft= new TableColumn("$ Left"); TableColumn pp= new TableColumn("$ PP"); 
        TableColumn r= new TableColumn("R"); TableColumn hr= new TableColumn("HR");TableColumn rbi= new TableColumn("RBI");
        TableColumn sb= new TableColumn("SB");TableColumn ba= new TableColumn("BA");TableColumn w= new TableColumn("W");
        TableColumn sv= new TableColumn("SV");TableColumn k= new TableColumn("K");TableColumn era= new TableColumn("ERA");
        TableColumn whip= new TableColumn("WHIP"); TableColumn totalpoints= new TableColumn("Total Points");
        standingsTable.getColumns().addAll(teamName,playersNeeded,moneyLeft,pp,r,hr,rbi,sb,ba,w,sv,k,era,whip,totalpoints);
        topWorkSpacePane.getChildren().add(standingsTable); playersNeeded.setMinWidth(100);
    
    }


// INIT A LABEL AND SET IT'S STYLESHEET CLASS
    private Label initLabel(PropertyType labelProperty, String styleClass) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String labelText = props.getProperty(labelProperty);
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        return label;
    }
    private Label initChildLabel(Pane container, PropertyType labelProperty, String styleClass) {
        Label label = initLabel(labelProperty, styleClass);
        container.getChildren().add(label);
        return label;
    }
    public ScrollPane getWorkSpaceScrollPane(){
        return workSpaceScrollPane;
    }
}
